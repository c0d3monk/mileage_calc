#!/usr/bin/env python
from builtins import property

from db import *
from exceptions import *
from vehicle import *
import math

class Stats:
    """
    This class deals with Statistical data of a vehicle like
    its fuel reading, tank_capacity, km run, fuel input date
    and updating these details in the db
    """

    def __init__(self):
        self.__start_kms = 0
        self.__distance = 0
        self.__date = None
        self.__fuel_reading = 0
        self.__refuel_amt = 0
        self.vehicle = None
        self.price_per_ltr = 50
        self.__reg_no = None

    @property
    def start_kms(self):
        return self.__start_kms

    @start_kms.setter
    def start_kms(self, value):
        self.__start_kms = value

    @property
    def refuel_amt(self):
        return self.__refuel_amt

    @refuel_amt.setter
    def refuel_amt(self, value):
        self.__refuel_amt = value

    @property
    def date(self):
        return self.__date

    @date.setter
    def date(self, value):
        self.__date = value

    @property
    def fuel_reading(self):
        return self.__fuel_reading

    @fuel_reading.setter
    def fuel_reading(self, value):
        # Fuel reading considered to be in percentage
        self.__fuel_reading = value

    @property
    def reg_no(self):
        return self.__reg_no

    @reg_no.setter
    def reg_no(self, value):
        self.__reg_no = value

    # TODO: This method should update the vehicle statistics matching the reg no
    def update_details(self, **params):
        valid = self.validate_params(**params)
        if valid:
            table = MTable()
            self.reg_no = params.pop("reg_no")
            self.vehicle = Vehicle(reg_no=self.reg_no)
            params['fuel_reading_after'] = self.calculate_fuel_reading_after_recharge(tc=self.vehicle.get_tc(),
                                                                                      refuel_amt=params.get('refuel_amt'),
                                                                                      fuel_reading=params.get('fuel_reading'))
            rows = table.select(tablename=self.reg_no)
            if rows.fetchone() is not None:
                all_data = table.select_all(self.reg_no)
                print(all_data[-1])
                print("Last km reading")
                last_km_reading = all_data[-1][2]
                print(last_km_reading)
                print("Last fuel reading")
                last_fuel_reading = all_data[-1][5]
                print(last_fuel_reading)
                params["mileage"] = self.calculate_mileage(last_fuel_reading, last_km_reading, **params)
            else:
                print("Setting mielage to 0")
                params["mileage"] = 0
            print("fuel_reading_after")
            print(params["fuel_reading_after"])
            print(params)
            table.insert(self.reg_no, **params)

    def calculate_fuel_reading_after_recharge(self, **params):
        print(str(params))
        fuel_added = int(params.get("refuel_amt"))/int(self.price_per_ltr)
        fuel_present = (int(params.get("fuel_reading"))/100)*(int(params.get("tc")))
        total_ltr = fuel_added + fuel_present
        new_fuel_reading = (total_ltr/int(params.get("tc")))*100
        return new_fuel_reading

    def calculate_mileage(self, last_fuel_reading=None, last_km_reading=None, **params):
        # TODO: mileage calculation
        print("params")
        print(params)
        distance = int(params.get("km_reading")) - int(last_km_reading)
        print("distance")
        print(distance)
        fuel_consumed_percent = int(last_fuel_reading) - int(params.get("fuel_reading"))
        print("fuel_consumed_percent")
        print(fuel_consumed_percent)
        f_percent = int(fuel_consumed_percent)/100
        print("f_percent")
        print(f_percent)
        tc = self.vehicle.get_tc()
        print("tank capacity")
        print(int(tc))
        fuel_consumed = f_percent * int(tc)
        print("fuel_consumed")
        print(fuel_consumed)
        mileage = int(distance/fuel_consumed)
        return mileage

    def validate_params(self, **kwargs):
        if kwargs is not None:
            reg_no = kwargs.get("reg_no", None)
            if not reg_no:
                raise ParamValidationError("Registration number is mandatory")
            else:
                return True
        else:
            raise InvalidNumberOfArguments


