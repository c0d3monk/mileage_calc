

from exceptions import *
from sqlalchemy import create_engine, MetaData, Table, Column, Date, Integer, exc, select, asc, desc
import re


# TODO: Write db operation wrappers
class Database:
    """
    This class deals with initializing and connecting to the db.
    """

    def __init__(self):
        self.db_uri = 'mysql+pymysql://arout:password@localhost/mcalc'
        self.engine = create_engine(self.db_uri)
        self.conn = self.engine.connect()
        self.metadata = MetaData()


class MTable(Database):
    """
    This class deals with the various table operations
    """

    def __init__(self):
        super().__init__()
        self.table_list = None

    def get_all_tables(self):
        self.table_list = self.engine.table_names()
        return str(self.table_list)

    def fetch_table_data(self, tablename=None):
        if tablename:
            try:
                table_obj = Table(tablename, self.metadata, autoload=True, autoload_with=self.engine)
                return str(repr(table_obj))
            except exc.NoSuchTableError:
                raise TableNotFounderror(str(tablename) + "not found")
        else:
            raise ParamValidationError("Table name is mandatory")

    def add_table(self, tablename=None):
        if tablename:
            table = Table(tablename, self.metadata,
                          Column('id', Integer, autoincrement=True, primary_key=True),
                          Column('date', Integer),
                          Column('km_reading', Integer),
                          Column('fuel_reading', Integer),
                          Column('refuel_amt', Integer),
                          Column('fuel_reading_after', Integer),
                          Column('mileage', Integer),
            )
            table.create(self.engine)
            return True
        else:
            raise ParamValidationError("Table name is mandatory")

    def delete(self, tablename=None):
        if tablename:
            table_obj = Table(tablename, self.metadata, autoload=True, autoload_with=self.engine)
            try:
                response = table_obj.drop(bind=self.engine)
            except exc.NoSuchTableError:
                raise TableNotFounderror(str(tablename) + "not found")
        else:
            raise ParamValidationError("Table name is mandatory")

    def insert(self, tablename=None, **params):
        if tablename:
            table_obj = Table(tablename, self.metadata, autoload=True, autoload_with=self.engine)
            try:
                ins = table_obj.insert().values(**params)
                self.conn.execute(ins)
            except exc.NoSuchTableError:
                raise TableNotFounderror(str(tablename) + "not found")
        else:
            raise ParamValidationError("Table name is mandatory")

    def update(self, tablename=None, k=None, v=None, **params):
        if tablename:
            table_obj = Table(tablename, self.metadata, autoload=True, autoload_with=self.engine)
            try:
                update = table_obj.update().where(table_obj.c[k] == v).values(**params)
                self.conn.execute(update)
            except exc.SQLAlchemyError:
                raise UnknownError("Unknown error", exc.SQLAlchemyError)
        else:
            raise ParamValidationError("Table name is mandatory")

    def select(self, **kwargs):
        k = kwargs.get('key', None)
        v = kwargs.get('value', None)
        column = kwargs.get('column', None)
        tablename = kwargs.get('tablename', None)
        if tablename is not None:
            table_obj = Table(kwargs.get('tablename'), self.metadata, autoload=True, autoload_with=self.engine)
            try:
                if k and v:
                    if column:
                        s = select([table_obj.c[column]]).where(table_obj.c[k] == v)
                    else:
                        s = select([table_obj]).where(table_obj.c[k] == v)

                else:
                    if column:
                        if re.search(",", column):
                            cols = re.split(",", column)
                            columns = []
                            for c in cols:
                                columns.append(table_obj.c[c])
                            s = select(columns)
                        else:
                            s = select([table_obj.c[column]])
                    else:
                        s = select([table_obj])
                print(s.compile(compile_kwargs={"literals_binds": True}))
                rows = self.conn.execute(s)
                return rows

            except exc.SQLAlchemyError:
                raise UnknownError("", exc.SQLAlchemyError)
        else:
            raise ParamValidationError("Table name is mandatory")

    def select_all(self, tablename=None):
        rows = self.select(tablename=tablename)
        return rows.fetchall()

    def has_table(self, tablename=None):
        if tablename:
            if self.engine.has_table(tablename):
                return True
            else:
                return False
        else:
            raise ParamValidationError("Table name is mandatory")