
class Error(Exception):
    """
    This is custom Error class
    """

    def __init__(self, msg, errors=None):
        super().__init__(msg)
        self.errors = errors


class InvalidNumberOfArguments(Error):
    pass

class ParamValidationError(Error):
    pass

class TableNotFounderror(Error):
    pass

class UnknownError(Error):
    pass

class VehicleNotRegistered(Error):
    pass