from exceptions import *
from db import MTable

class Vehicle:
    """
    This class deals with all the vehicle information. It has methods to add/delete or fetch
    a vehicle information based on the vehicle registration number
    """

    def __init__(self, **params):
        self.__vtype = params.get("type", None)
        self.__model = params.get("model", None)
        self.__reg_no = params.get("reg_no", None)
        self.__tank_capacity = params.get("tank_capacity", None)
        self.mtable = MTable()

    @property
    def vtype(self):
        return self.__vtype

    @vtype.setter
    def vtype(self, value):
        self.__vtype = value

    @property
    def model(self):
        return self.__model

    @model.setter
    def model(self, value):
        self.__model = value

    @property
    def tank_capacity(self):
        return self.__tank_capacity

    @property
    def reg_no(self):
        return self.__reg_no

    @reg_no.setter
    def reg_no(self, value):
        self.__reg_no = value

    # TODO: Write logic for vehicle db operations

    def add_vehicle(self):
        self.mtable.insert("vehicle_details", type=self.vtype, model=self.model,
                           tank_capacity=self.tank_capacity, reg_no=self.reg_no)
        self.mtable.add_table(self.reg_no)

    def del_vehicle(self):
        pass

    def get_vehicle(self, **params):
        pass

    def get_tc(self):
        rows = self.mtable.select(tablename="vehicle_details", key="reg_no", value=self.reg_no, column="tank_capacity")
        row = rows.fetchone()
        return row["tank_capacity"]

    def check_vehicle_registered(self):
        rows = self.mtable.select(tablename="vehicle_details", key="reg_no", value=self.reg_no,
                               column="reg_no")
        row = rows.fetchone()
        if row and row["reg_no"] == self.reg_no:
            print("returing true")
            return True
        else:
            print("returning false")
            return False

    def check_stats_table(self):
        if self.mtable.has_table(self.reg_no):
            return True
        else:
            return False

    def validate_params(self, **kwargs):
        if kwargs is not None:
            reg_no = kwargs.get("reg_no", None)
            if not reg_no:
                raise ParamValidationError("Registration number is mandatory")
        else:
            raise InvalidNumberOfArguments

