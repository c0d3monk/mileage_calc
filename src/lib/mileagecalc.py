#!/usr/bin/env python

import pymysql
from stats import Stats
from vehicle import Vehicle
from exceptions import *
from analytics import Analytics


class Mileage:
    """
    This class deals with taking the user data, computing the stats and
    generating a report
    """

    def __init__(self):
        self.valid = None
        self.analytics = Analytics()
        self.stats = None
        self.vehicle = None

    def set_vehicle_details(self, **params):
        self.validate_params(**params)
        self.vehicle = Vehicle(**params)
        if not self.vehicle.check_vehicle_registered():
                self.vehicle.add_vehicle()

    def update_stats(self, **params):
        valid = self.validate_params(**params)
        if valid:
            self.vehicle = Vehicle(**params)
            if self.vehicle.check_stats_table():
                self.stats = Stats()
                self.stats.update_details(**params)
            else:
                raise VehicleNotRegistered(str(params.get("reg_no")) + " stats table missing")

    def gen_report(self, **params):
        self.analytics.gen_report(**params)

    def validate_params(self, **kwargs):
        if kwargs is not None:
            reg_no = kwargs.get("reg_no", None)
            if not reg_no:
                raise ParamValidationError("Registration number is mandatory")
            else:
                return True
        else:
            raise InvalidNumberOfArguments
