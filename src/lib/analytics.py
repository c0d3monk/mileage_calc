from exceptions import *
from db import *

# TODO: Get the analytics done
class Analytics:
    """
    This class deals with data analytics and report generation
    """

    def __init__(self):
        pass

    def gen_report(self, **params):
        if not params:
            raise InvalidNumberOfArguments
        else:
            table = MTable()
            tb_data = table.select(tablename=params.get('reg_no', None), column='date,mileage')
            print("Table data :")
            m_array = print(tb_data.fetchall())

