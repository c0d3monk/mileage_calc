#!/usr/bin/env python

from mileagecalc import *
from db import *

if __name__ == '__main__':
    mileage = Mileage()
    mileage.set_vehicle_details(type="Car", model="Dzire", tank_capacity=121, reg_no="KA1234")
    mileage.update_stats(km_reading="32000", refuel_amt=3100, fuel_reading=10, date="1212121212", reg_no="KA1234")
    mileage.update_stats(km_reading="32400", refuel_amt="2000", fuel_reading="40", date="11111111", reg_no="KA1234")
    mileage.update_stats(km_reading="32800", refuel_amt="2000", fuel_reading="40", date="11111111", reg_no="KA1234")
    mileage.update_stats(km_reading="33200", refuel_amt="2000", fuel_reading="40", date="11111111", reg_no="KA1234")
    mileage.update_stats(km_reading="33600", refuel_amt="2000", fuel_reading="40", date="11111111", reg_no="KA1234")
    mileage.analytics.gen_report(reg_no="KA1234")
    # mileage.gen_report(reg_no="1234")
    #table = MTable()
    #table.add("KA1234")
    #data = table.delete("KA1234")
    # print(table.get_all_tables())
    #table.insert('KA1234', id='1', date='12344', km_reading='111', fuel_reading='333', refuel_amt='123')
    #table.select('KA1234', 'id', 1)
    #print(data)